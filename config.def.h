static const Block blocks[] = {
	/* icon   command                  update interval   update signal */
	{ "",     "bar_network",           30,               0 },
	{ "",     "bar_autolock",          0,                1 },
	{ "",     "bar_keyboard_layout",   0,                2 },
	{ "",     "bar_volume",            60,               3 },
	{ "",     "bar_memory",            30,               0 },
	{ "",     "bar_date",              60,               0 },
	{ "",     "bar_battery",           5,                0 },
};

// sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "^d^|";
static unsigned int delimLen = 4;
